package site.barsukov.barcodefx.context;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ProductionContext extends BaseContext {
    private String productionOrder;
    private String certificateType;
    private LocalDate productDate;
    private String participantInn;
    private String producerInn;
    private String ownerInn;
    private String certificateNumber;
    private LocalDate certificateDate;
    private String tnvedCode;
    private String vsdNumber;
}
